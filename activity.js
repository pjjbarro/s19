let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}
let sentence4 = `Hi, I'm ${student1.name}`;
let sentence5 = `I was born on ${student1.birthday}`;
let sentence6 = `I am ${student1.age} years old.`;
let sentence7 = `I am ${student1.isEnrolled}`;
let sentence8 = `My classes are ${student1.classes}`;

console.log(sentence4);
console.log(sentence5);
console.log(sentence6);
console.log(sentence7);
console.log(sentence8);

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401","Natural Sciences 402"]
}
let sentence9 = `Hi, I'm ${student2.name}`;
let sentence10 = `I was born on ${student2.birthday}`;
let sentence11 = `I am ${student2.age} years old.`;
let sentence12 = `I am ${student2.isEnrolled}`;
let sentence13 = `My classes are ${student2.classes}`;

console.log(sentence9);
console.log(sentence10);
console.log(sentence11);
console.log(sentence12);
console.log(sentence13);

function introduce(student){

	console.log('Hi, Im ${student.name} I am  ${student.age} years old.');
	console.log(`I study the following courses ${student.classes}`);
}

function getCube(num){

	console.log(Math.pow(num,3));

}

let cube = getCube(3);

console.log(cube)

let numArr = [15,16,32,21,21,2];

numArr.forEach = (num) => {

	console.log(num);
}

let numsSquared = numArr.map(num => num ** 2)
console.log(numsSquared);


class Dog {
	constructor(name,breed,age){
		this.name = name;
		this.breed = breed;
		this.age = age;
	}
}

let dog2 = new Dog ("Basty","Poodle","14");
console.log(dog2);
let dog3 = new Dog ("Toby","German Shepperd","16");
console.log(dog3);
let dog4 = new Dog ("Ellie","Shih tzu","18");
console.log(dog4);