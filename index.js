








let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "Shows";
let string5 = "Teaches";
let string6 = "Javascript";
let string7 = "Typescript";

let sentence = '${string1} ${string2} ${string3 ${string4} ${string5} ${string6}.'
// ${} - allows you to embed variables or JS expressions: placeholder(${}).
/*let fivePowerOf1 = Math.pow(5,3);*/
// Math.pow allows us to get the result of a number raised to given exponent 
// Math.pow(base,exponent)
console.log(sentence);
/*console.log(fivePowerOf3);*/

// Template Literals are part of JS ES6 Updates

// Exponent operators ** - allow us to get the result of a number raised to a given exponent. It is used as alternative to Math.pow()

let fivePowerOf2 = 5 ** 2;
console.log(fivePowerOf2);//25

let fivePowerOf3 = 5 ** 3;
console.log(fivePowerOf3);//125

let squareRootOf4 = 4 ** .5;
console.log(squareRootOf4);//2

// Template literal with JS expressions
let sentence2 = 'The result of five to the power of 2 is ${5**2}';
console.log(sentence2)

// Mini-Activity

let sentence3 = 'The result of 25 to the power of 2 is $(fivePowerOf2 ** 2}'
console.log(sentence3);

// Array Destructuring - this will allow us to save array items in variables.

let array = ["Curry","Thompson","Green","Iguodala"];

console.log(array[2]);//display green in console
let warriorPlayer1 = array[3];
console.log(warriorPlayer1);
let warriorPlayer2 = array[0];
console.log(warriorPlayer2);

const [curry,thompson,green] = array;

console.log(curry);
console.log(thompson);
console.log(green);

let array2 = ["Curry","Lillard","Paul","Irving"]
// Mini-activity

const [pointGuard1,pointGuard2,pointGuard3,pointGuard4] = array2

console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

let array3 = ["Jokic","Embiid","Howard","Anthony-Towns"];
//In arrays, order matters and that goes the same to our destructuring
//You can skip an item by adding another separator (,) but no variable name.

let [center1,center3,center4] = array3;
//Destructuring can allow us to both destructure an array into let or const variables.
//syntax: let/const [variable1,variable2] = array; 


console.log(center1)
console.log(center3)
console.log(center4)

center1 = "Shaquille O' Neal";
console.log(center1);

//Object Destructuring will allow us to destructure an object by allowing to add the values of an object's property into respective variables.

let person = {
	name: "Paul Phoenix",
	birthday: "August 5, 1995",
	age: 26
};

//Mini-Activity
let sentence4 = 'Hi, Im ${person.name}';
let sentence5 = 'I was born on ${person.birthday}';
let sentence6 = 'I am ${person.age} years old';

console.log(sentence4);
console.log(sentence5);
console.log(sentence6);

const {age,name,birthday} = person;

/*

	Object Destructuring

	In object destructuring we have to match the variable names with the properties that the object has. It doesn't matter in what order our variables are in.

*/

console.log(age);
console.log(name);//person.firstName is undefined. There is no firstName property in person object.
console.log(birthday);

//Mini-Activity

let pokemon1 = {

	name: "Charmander",
	level: 11,
	type: "Fire",
	moves: ["Ember","Scratch","Leer"]
}
const {level,type,moves} = pokemon1;
let sentence7 = 'My pokemon is ${name}, it is level ${level}. It is a ${type}. Its moves are ${moves}'

console.log(moves)//array ["Ember","Scratch","Leer"]

const [move1,,move3] = moves;
console.log(move1);
console.log(move3);
//After destructuring, the variables are independent from the original array/object.
pokemon1.name = "Bulbasaur";
console.log(name);
console.log(pokemon1);

//Arrow functions
	//Arrow functions are an alternative way of writing functions in Javascript. However, arrow function have significant pros and cons against the use of traditional functions.

	//traditional function
	function displayMsg(){
		console.log('Hello, World!');
	};

	//arrow function
	const hello = () => {
		console.log("Hello from Arrow");
	}

	const greet = (personParams) => {

		console.log('Hi, ${personParams.name!}')
	};

	greet(person);

	//Implicit return - allows us to return a value without the use of return keyword. Arrow functions can do this while Traditional Functions.

	function addNum(num1,num2){
		console.log(num1 + num2);
	}

	const subtractNum = (num1,num2) => num1 - num2;

	let sum = addNum(5.6);
	let difference = subtractNum(12,6);
	
	console.log(sum);//Because addNum() has no return keyword, it is not able to return the result of num1+num2
	console.log(difference);//Even without a return keyword, arrow functions can return a value. SO long as its code block is not wrapped with {}

	//this keyword in a method

	let protagonist = {

		name: "Cloud Strife",
		occupation: "Soldier",
		greet: function(){
			//traditional methods would have this keyword refer to the parent object
			console.log(this);
			console.log('Hi!,im ${this.name}.')
		},
		introduceJob: () => {
			//When inside an arrow function used as a method in an object
			//The this keyword does not refer to the parent object but instead the the global window.
			console.log(this);
			console.log('I work as ${this.occupation}')
		}
	}

	protagonist.greet();
	protagonist.introduceJob();

	//Class-Base object blueprints
		//In javascript, classes are templates of objects 
		//We can create objects out of the use of classes
		//Before the introduction of classes in JS, we mimic this beahvior or this ability to create objects out of templates with use of: construction functions

		// Create a class
			//The constructor is a special method for creating and initializing an object

		//sample of construction function
		function Pokemon(name,type,level){
			this.name = name;
			this.type = type;
			this.level = level;
		}

		//ES6 Class Creation

		class Car {
			constructor(brand,name,year){
				this.brand = brand;
				this.name = name;
				this.year = year;
			}
		}


		let car1 = new Car("Toyota","Vios","2002");
		console.log(car1);
		let car2 = new Car("Cooper","Mini","1969");
		console.log(car2)
		let car3 = new Car("Porsche","911","1960");
		console.log(car3)